<?php

ini_set('display_errors', 1);
// Root path for inclusion.
define('ROOT', __DIR__);


// Require composer autoloader
require_once ROOT . '/vendor/autoload.php';

// Require database component
require_once ROOT . '/app/database.php';

global $capsule;
$capsule->connection()->insert('
    
    CREATE TABLE IF NOT EXISTS `sites` (
      `id` INT NOT NULL AUTO_INCREMENT,
      `slug` VARCHAR(45) NULL,
      `site` VARCHAR(255) NULL,
      `created_at` INT(11) NULL,
      `visit` INT(11) NULL,
      PRIMARY KEY (`id`))
    ENGINE = InnoDB;
    
');