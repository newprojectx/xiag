<?php

use app\models\Sites;

/* errors on */
ini_set('display_errors', 1);

/* require vendor and database connect */
require_once 'app/start.php';

global $capsule;
$capsule->setFetchMode(PDO::FETCH_OBJ);
$db = $capsule->getConnection();

if (@$_GET['url']) {

    $uri = explode('/', $_GET['url']);

    $id = 0;
    if (isset($uri[1]))
        $id = $uri[1];

    if ($_SERVER['REQUEST_METHOD'] == 'GET' && $uri[0] == 'api' && !$id)
    {
        $rows = $db->table('sites')->get();
        foreach ($rows as $row) {
            $row->created_at = date("Y-m-d H:i:s", $row->created_at);
        }
        echo json_encode($rows);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && $uri[0] == 'api' && !$id)
    {
        $newSite = json_decode(file_get_contents('php://input'));

        $slug = uniqid();

        $db->table('sites')->insert([
            'slug' => $slug,
            'site' => $newSite->site,
            'created_at' => time(),
            'visit' => 0
        ]);

        $row = $db->table('sites')->select('*')->where('slug', '=', $slug)->first();
        $row->created_at = date("Y-m-d H:i:s", $row->created_at);
        echo json_encode($row);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] == 'DELETE' && $uri[0] == 'api' && $id)
    {
        $db->table('sites')->delete($id);
        exit;
    }

    /* REDIRECT OR NOT FOUND */
    $row = Sites::where('slug', '=', $_GET['url'])->first();

    $location = '';

    if ($row)
    {
        $row->visit++;
        $row->save();
        $http = substr($row->site, 0, 4) == 'http';

        if ($http) {
            $location = $row->site;
        } else {
            $location = "http://{$row->site}/";
        }
    }

    if (filter_var($location, FILTER_VALIDATE_URL)) {
        header("Location: {$location}");
        exit;
    }
    else
    {
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        include("404.php");
        exit;
    }
}
?>
<!doctype html>
<html lang="en" ng-app="exampleApp">
<head>
    <meta charset="UTF-8">
    <title>Sort link url</title>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular.min.js"></script>
    <script src="app/assets/js/app.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body ng-controller="defaultCtrl">
<div class="panel panel-primary">
    <h3 class="panel-heading">Sites</h3>
    <ng-include src="'views/table.html'" ng-show="currentView == 'table'"></ng-include>
    <ng-include src="'views/edit.html'" ng-show="currentView == 'edit'"></ng-include>
</div>
</body>