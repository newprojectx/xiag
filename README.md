
### System requirements
~~~bash
1) php 5.3 or better
2) mysql 5.5 or better
~~~bash

### Installing
~~~bash
Run ssh connection to the server

First step. From root projects directory run command:
git clone https: //newprojectx@bitbucket.org/newprojectx/xiag.git

Second step. Set up the database connection in file "app/database.php"

In the project folder, follow commands:
1) composer install
2) php init.php
3) create nginx or apache config file for site and do restarting server
~~~bash

### More information

~~~bash
Composer install - installing dependences, for active record query

File "init.php" - include sql command, create "sites" table in database
~~~bash