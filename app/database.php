<?php

use Illuminate\Database\Capsule\Manager as Capsule;

global $capsule;
$capsule = new Capsule();

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => '127.0.0.1',
    'database'  => 'ih2121u6_betinaf',
    'username'  => 'ih2121u6_betinaf',
    'password'  => '13BpI1q7',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => ''
]);

$capsule->setAsGlobal();

$capsule->bootEloquent();
