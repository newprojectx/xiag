<?php

namespace app\models;

use Illuminate\Database\Eloquent\Model;

class Sites extends Model
{
    protected $table = 'sites';

    public $timestamps = false;
}