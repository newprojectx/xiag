<?php

// Root path for inclusion.
define('INC_ROOT', dirname(__DIR__));

spl_autoload_register(function ($class) {
    $path = explode('\\', $class);
    include __DIR__ . '/models/' . end($path) . '.php';
});

// Require composer autoloader
require_once INC_ROOT . '/vendor/autoload.php';


// Require database component
require_once INC_ROOT . '/app/database.php';
