﻿angular.module("exampleApp", [])
.constant("baseUrl", "http://betinafei.ru/api")
.controller("defaultCtrl", function ($scope, $http, baseUrl) {

    // default view
    $scope.currentView = "table";

    // find all rows in table
    $scope.refresh = function () {
        // HTTP GET
        $http.get(baseUrl).success(function (data) {
            $scope.items = data;
        });
    }

    // create new row
    $scope.create = function (item) {
        // HTTP POST
        $http.post(baseUrl, item).success(function (data) {
            console.log(data);
            $scope.items.push(data);
            $scope.currentView = "table";
        });
    }

    // delete row
    $scope.delete = function (item) {
        // HTTP DELETE
        $http({
            method: "DELETE",
            url: baseUrl + '/' + item.id
        }).success(function () {
            $scope.items.splice($scope.items.indexOf(item), 1);
        });
    }

    // edit or create
    $scope.editOrCreate = function (item) {
        $scope.currentItem = item ? angular.copy(item) : {};
        $scope.currentView = "edit";
    }

    // save
    $scope.saveEdit = function (item) {
        if (angular.isDefined(item.id)) {
            $scope.update(item);
        } else {
            $scope.create(item);
        }
    }

    // cancel
    $scope.cancelEdit = function () {
        $scope.currentItem = {};
        $scope.currentView = "table";
    }

    $scope.refresh();
});